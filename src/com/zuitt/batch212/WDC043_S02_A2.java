package com.zuitt.batch212;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class WDC043_S02_A2 {
    public static void main(String[] args) {
        int [] firstFivePrimeNumbers = new int[5];

        firstFivePrimeNumbers[0] = 2;
        firstFivePrimeNumbers[1] = 3;
        firstFivePrimeNumbers[2] = 5;
        firstFivePrimeNumbers[3] = 7;
        firstFivePrimeNumbers[4] = 11;

        System.out.println("the first prime number is: " + firstFivePrimeNumbers[0]);
        System.out.println("the second prime number is: " + firstFivePrimeNumbers[1]);
        System.out.println("the third prime number is: " + firstFivePrimeNumbers[2]);
        System.out.println("the fourth prime number is: " + firstFivePrimeNumbers[3]);
        System.out.println("the fifth prime number is: " + firstFivePrimeNumbers[4]);

        ArrayList<String> friends = new ArrayList<>();
        friends.add("Yuji");
        friends.add("Nobara");
        friends.add("Megumi");
        friends.add("Gojo");

        System.out.println("my friends are: " + friends);

        HashMap<String, Integer> inventory = new HashMap<>();

        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);

        System.out.println("Our current inventory consist of: " + inventory);
    }
}
