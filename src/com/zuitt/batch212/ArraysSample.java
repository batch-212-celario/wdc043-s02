package com.zuitt.batch212;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class ArraysSample {
    public static void main(String[] args) {
     // Arrays = fixed/limited collections of data
        // Declaration
        int[] intArray = new int[3];

        intArray[0] = 10;
        intArray[1] = 20;
        intArray[2] = 30;

        System.out.println(intArray[2]);
        // other syntax used to declare arrays
        int intSample[] = new int[3];
        intSample[0] = 50;
        System.out.println(intSample[0]);
        //String Array
        String stringArray[] = new String[3];

        stringArray[0] = "john";
        stringArray[1] = "jane";
        stringArray[2] = "Joe";
        //Declaration with initialization
        int[] intArray2 = {100, 200, 300, 400, 500};
        System.out.println(intArray2);

        System.out.println(Arrays.toString(intArray2));
        System.out.println(Arrays.toString(stringArray));

        //method used in arrays
        // sort()
        Arrays.sort(stringArray);
        System.out.println(Arrays.toString(stringArray));

        //binary search
        String searchTerm = "pikachu";
        int binaryResult = Arrays.binarySearch(stringArray, searchTerm);
        System.out.println(binaryResult);
        // binarySearch searches the specified array of the given data type for the Arrays.sort() method prior to making this call. if it is not sorted the results are undefined

        //Multidimensional Array
        String[][] classroom = new String[3][3];
        //first row
        classroom[0][0] = "pikachu1";
        classroom[0][1] = "pikachu2";
        classroom[0][2] = "pikachu3";
        //second row
        classroom[1][0] = "pikachu4";
        classroom[1][1] = "pikachu5";
        classroom[1][2] = "pikachu6";
        //third row
        classroom[2][0] = "pikachu7";
        classroom[2][1] = "pikachu8";
        classroom[2][2] = "pikachu9";

        System.out.println(Arrays.deepToString(classroom));

        //Arraylist- resizeable arrays, wherein elements can be added or removed whenever it is needed.

        //declaration
        ArrayList<String> students = new ArrayList<>();
        //adding elements
        students.add("pikachu10");
        students.add("pikachu11");
        System.out.println(students);

        // Access elements to an ArrayList
        System.out.println(students.get(0));
        // update or changing of elements
        students.set(1, "the original pikachu");
        System.out.println(students);
        // removing elements
        students.remove(1);
        System.out.println(students);
        // removing all elements in an Arraylist using the clear method
        students.clear();
        System.out.println(students);
        // getting the number of elements in an Arraylist using the size method get the length of an array list
        System.out.println(students.size());

        // we can also declare and initialize a values
        ArrayList<String> employees = new ArrayList<>(Arrays.asList("pikachu12","pikachu13"));
        System.out.println(employees);


        // Hashmaps -> key: value pair
        HashMap<String,String> employeeRole = new HashMap<>();
        // Adding fields
        employeeRole.put("Captain", "Luffy");
        employeeRole.put("Doctor", "Chopper");
        employeeRole.put("Navigator", "Nami");
        System.out.println(employeeRole);
        //Retrieving field value
        System.out.println(employeeRole.get("Captain"));
        //Removing elements
        employeeRole.remove("Doctor");
        System.out.println(employeeRole);
        //Retrieving hashmap keys/fields
        System.out.println(employeeRole.keySet());
        // with integers as values
        HashMap<String, Integer> grades = new HashMap<>();

        grades.put("English", 89);
        grades.put("Math", 93);
        System.out.println(grades);

        // Hashmap with Array Lists
        HashMap<String, ArrayList<Integer>> subjectGrades = new HashMap<>();
        ArrayList<Integer> gradesListA = new ArrayList<>(Arrays.asList(75, 80, 90));
        ArrayList<Integer> gradeListB = new ArrayList<>(Arrays.asList(85, 87, 90));

        subjectGrades.put("Joe", gradesListA);
        subjectGrades.put("Jane", gradeListB);

        System.out.println(subjectGrades);


        //Operators
        //1. Arithmetic -> +, - , *, /, %;
        //2. Comparison -> >, <, >=, <=, ==, !=
        //3. Assignment -> =, +=
        //4. logical -> &&, ||, !
    }
}
