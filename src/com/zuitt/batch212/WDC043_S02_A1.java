package com.zuitt.batch212;

import java.util.Scanner;

public class WDC043_S02_A1 {
    public static void main(String[] args) {
        Scanner appScanner = new Scanner(System.in);

        System.out.println("Input year to be checked if leap year");
        int year = appScanner.nextInt();
        if (year % 4 !=0){
            System.out.println(year + " is not a leap year");
        } else if (year % 400 == 0){
            System.out.println(year + " is a leap year");
        } else if (year % 100 == 0){
            System.out.println(year + " is not a leap year");
        } else
            System.out.println(year + " is a leap year");
    }
}
